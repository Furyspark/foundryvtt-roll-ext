Hooks.once("init", async function() {
  game.settings.register("roll-ext", "hideRolls", {
    name: "Hide Rolls",
    hint: "Hide rolls completely for players who would otherwise receive a mystery roll notification.",
    scope: "world",
    config: true,
    type: Boolean,
    default: false
  });
});

DicePool.rgx.math = /(floor|ceil|round|abs)/;

// Hijack DicePool methods to insert more things
const DicePool__keepOrDrop = DicePool.prototype._keepOrDrop;
DicePool.prototype._keepOrDrop = function(results, mod) {
  this._alterRolls(results, mod);

  DicePool__keepOrDrop.call(this, results, mod);
};
DicePool.prototype._parseModifiers = function(modifiers) {
  let patterns = [DicePool.rgx.keep, DicePool.rgx.success].concat(this._moreModifiers());
  for ( let p of patterns ) {
    modifiers = modifiers.replace(RegExp(p, "g"), "$&;");
  }
  return modifiers.split(";").filter(m => m !== "");
};

DicePool.prototype._moreModifiers = function() {
  return [DicePool.rgx.math];
};
DicePool.prototype._alterRolls = function(results, mod) {
  this._math(results, mod);
};

DicePool.prototype._math = function(results, mod) {
  const re = mod.match(DicePool.rgx.math);
  if (!re) return;

  const mode = re[1];

  for (let r of results) {
    r.total = Math[mode](r.total);
  }
};

Object.defineProperties(ChatMessage.prototype, {
  visible: {
    get: function() {
      if ( this.data.whisper.length ) {
        const hideRolls = game.settings.get("roll-ext", "hideRolls");
        if ( this.data.type === CONST.CHAT_MESSAGE_TYPES.ROLL && !hideRolls ) return true;
        if ( (this.data.user === game.user._id) || this.data.whisper.indexOf(game.user._id ) !== -1 ) return true;
        let allowSecret = game.settings.get("core", "secretMessages");
        return ( !allowSecret && game.user.isGM );
      }
      return true;
    }
  }
});

Roll.prototype._evalParentheticalTerms = function(formula) {
  let terms = formula.replace(/\(/g, ";(;").replace(/\)/g, ";);").split(";").filter(o => o !== "");
  let nOpen = 0;
  let ignore = [];

  // Match parenthetical groups
  terms = terms.reduce((arr, t, a) => {
    if ( nOpen > 0 ) arr[arr.length - 1] += t;
    else arr.push(t);
    if ( t === "(" ) {
      if (a > 0 && terms[a-1].match(/(?:floor|round|ceil|max|min|abs|sign)$/)) ignore.push(nOpen);
      else nOpen = Math.max(1, nOpen + 1);
    }
    if ( t === ")" ) {
      if (ignore.includes(nOpen)) ignore.splice(ignore.indexOf(nOpen, 1));
      else nOpen = Math.max(0, nOpen - 1);
    }
    return arr;
  }, []);

  // Convert parenthetical groups to inner Roll objects
  return terms.reduce((arr, t) => {
    if ( t === "" ) return arr;
    let pt = t.match(this.rgx.parenthetical);
    arr.push(pt ? new Roll(pt[1], this.data) : t);
    return arr;
  }, []);
};
