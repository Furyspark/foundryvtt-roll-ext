# Roll Extended

This Foundry VTT module adds the ability to roll nested die and adds an option
to hide GM rolls from players completely.

## Installation

Install using the url `https://gitlab.com/Furyspark/foundryvtt-roll-ext/raw/master/module.json`

## Nested Die

An example of nested die is `/roll (2d4)d(1d4+1)`, which will roll between 2 and
8 die with 2-5 sides.

## License

MIT
